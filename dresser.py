import glob
import re
from elftools.elf.elffile import ELFFile
from capstone import *

class Dresser(object):
    def __init__(self, f):
        try:
            self.e = ELFFile(f)
        except:
            self.e = None
        self.mode = CS_MODE_64
        self.base = 0
        return

    def is_stripped(self):
        if self.e.get_section_by_name('.symtab') is None:
            return True
        else:
            return False

    def general_check(self):
        if self.e is None:
            return "Could not load binary."
        if not self.is_stripped():
            return "The binary is not stripped. Have a nice day."
        return

    def get_functions(self):
        code = self.e.get_section_by_name('.text')
        ops = code.data()
        self.base = code['sh_addr']
        cs = Cs(CS_ARCH_X86, self.mode)

        functionList = {}
        done_size = 0
        f_start = 0
        done_function = False
        for inst in cs.disasm(ops, self.base):
            done_size += len(inst.bytes)
            if inst.mnemonic == 'call':
                if inst.op_str[:2] != '0x': continue
                target = int(inst.op_str, 16)
                if self.base <= target <= self.base + code.data_size:
                    offset = target - self.base
                    if offset not in functionList:
                        size = self.get_size(offset)
                        functionList[offset] = ops[offset:offset + size]
            elif inst.mnemonic == 'ret' or inst.op_str == 'ret' or (inst.mnemonic == 'nop' and inst.op_str != ''):
                if f_start not in functionList:
                    functionList[f_start] = ops[f_start:done_size]
                f_start = done_size
                done_function = True
            
            if done_function:
                if inst.mnemonic == 'nop':
                    f_start = done_size
                else:
                    done_function = True

        return functionList

    def get_size(self, address):
        code = self.e.get_section_by_name('.text')
        ops = code.data()
        base = code['sh_addr']
        cs = Cs(CS_ARCH_X86, self.mode)

        size = 0
        for inst in cs.disasm(ops[address:address + 0x200], address):
            size += len(inst.bytes)
            if inst.mnemonic == 'ret' or inst.op_str == 'ret':
                break
        
        return size

    def identify(self, functionList):
        max_count, possible_libc = 0, None
        for libpath in glob.glob("./static/db/*.so"):
            libc_name = re.findall("\./static/db/(.+)\.so", libpath)[0]
            with open(libpath, "rb") as f:
                libc = f.read()

            count = 0
            for offset in functionList:
                if functionList[offset] in libc:
                    count += 1

            if count > max_count:
                possible_libc = libc_name
                max_count = count
        
        return max_count, possible_libc

    def dress(self, functionList, libc_name):
        result = {}

        sympath = "./static/db/{}.symbols".format(libc_name)
        symtable = {}
        with open(sympath, "r") as f:
            for line in f:
                symbol, address = line.split()
                symtable[int(address, 16)] = symbol
        
        libpath = "./static/db/{}.so".format(libc_name)
        with open(libpath, "rb") as f:
            libc = f.read()

        for offset in functionList:
            x = libc.find(functionList[offset])
            if x <= 0: continue
            
            if x in symtable:
                result[self.base + offset] = symtable[x]
                
        return result

    def genIDC(self):
        result = ''
        
        err = self.general_check()
        if err is not None:
            return None, err

        functionList = self.get_functions()
        #count, libc = self.identify(functionList)
        count, libc = 77, "libc6_2.27-3ubuntu1_amd64"
        print(count, libc)

        result += '/* {} ({} functions matched) */\n'.format(libc, count)

        symbols = self.dress(functionList, libc)
        for addr in symbols:
            result += 'create_insn({});\n'.format(hex(addr))
            result += 'set_name({}, "{}");\n'.format(hex(addr), symbols[addr])
        
        return result, None

    def genELF(self):
        result = b''
        return result, None

if __name__ == '__main__':
    # test
    with open("../dresser/debris/bin227.stripped", "rb") as f:
        dress = Dresser(f)
        print(dress.genIDC())
