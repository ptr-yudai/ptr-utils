import glob
import re

class LibcDB(object):
    def __init__(self):
        
        return

    def find_single(self, symbol, address):
        libcList = set()
        for sympath in glob.glob("./static/db/*.symbols"):
            libc_name = re.findall("\./static/db/(.+)\.symbols", sympath)[0]
            with open(sympath, "r") as f:
                for line in f:
                    symbol_t, address_t = line.split()
                    address_t = int(address_t, 16)
                    if symbol_t == symbol and hex(address_t)[-3:] == hex(address)[-3:]:
                        libcList.add(libc_name)
        return libcList

    def find(self, symbolList, addressList):
        libcList = None
        for symbol, address in zip(symbolList, addressList):
            try:
                address = int(address, 16)
            except ValueError:
                return None, "Invalid hex address: {}".format(address)
            result = self.find_single(symbol, address)
            if libcList is None:
                libcList = result
            else:
                libcList &= result
            if len(libcList) == 0:
                return None, "Matching libc not found".format(symbol)
        else:
            return libcList, None
