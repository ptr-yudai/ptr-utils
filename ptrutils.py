#!/usr/bin/env python
import os
from flask import Flask, render_template, flash, request, make_response
from libcdb import LibcDB
from dresser import Dresser

app = Flask(__name__)
app.secret_key = os.urandom(16)

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/libcdb', methods=['GET'])
def libcdb():
    symbolList = request.args.getlist('symbol')
    addressList = request.args.getlist('address')
    if len(symbolList) != len(addressList):
        flash("Invalid input")
    else:
        db = LibcDB()
        libcList, err = db.find(symbolList, addressList)
        if err is None:
            return render_template('libcdb.html', libcList=libcList)
        else:
            flash(err)
    return render_template('libcdb.html')

@app.route('/dresser', methods=['GET', 'POST'])
def dresser():
    if request.method == 'POST':
        binary = request.files['binary']
        if binary:
            dress = Dresser(binary)
            print(request.form['address'])
            if 'symbol' in request.form:
                result, err = dress.genIDC()
                if err:
                    flash(err)
                else:
                    response = make_response()
                    response.data = result
                    response.headers['Content-Disposition'] = 'attachment; filename=symbol_' + binary.filename + '.idc'
                    response.mimetype = "application/octet-stream"
                    return response
            elif 'binary' in request.form:
                flash("Not supported yet!")
        else:
            flash("Empty file")
    return render_template('dresser.html')

if __name__ == '__main__':
    app.run(
        debug = True,
        host = '0.0.0.0',
        port = 8001
    )
